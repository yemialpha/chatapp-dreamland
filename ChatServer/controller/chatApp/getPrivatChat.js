const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const ChatAppUser = require("../../models/ChatAppUser");
const Chat = require("../../models/Chat");



module.exports = async (req, res, next) => {

    let chatId  = ()=>{
        let arv =[`${req.userId}`, `${req.body.chatWith}`]
        let sortArv = arv.sort()
        return sortArv.join('')
        
    }

    

    try {
        const currentPage = req.query.page || 1;
        const perPage = 10;
        let user = await ChatAppUser.findById(req.userId)
        let totalItems = await Chat.find({chatId: chatId()}).countDocuments()
        let chat = await Chat.find({chatId: chatId()}).skip((currentPage - 1) * perPage)
        .limit(perPage).sort({updatedAt: -1})

        res.status(200).json({
            message: "Successful",
            status: "OK",
            chat: chat,
            totalItems
          });


       

        

        let result = user.chatNotification.filter(
            element => { return element.chatId != chatId()}
            );

        user.chatNotification = result
        user.save()


    } catch (err) {
        if (!err.statusCode) {
          err.statusCode = 500;
        }
        next(err);
      }
}