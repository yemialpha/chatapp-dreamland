const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const io = require("../../webSocketConfig");

const ChatAppUser = require("../../models/ChatAppUser");
const { check, validationResult } = require("express-validator");

const { RemoveUser,} = require("./OnlineUser")



module.exports = async (req, res, next) => {

    RemoveUser(req.userId)

    res.status(200).json({
        message: "Successfully Logout",
      });

    let opt={
        type:"remove",
        _id:req.userId
    }

      io.getIO().emit('Notification/online', {
        data: opt
    })

}