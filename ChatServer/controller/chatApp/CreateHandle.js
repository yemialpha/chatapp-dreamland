const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const ChatAppUser = require("../../models/ChatAppUser");
const { check, validationResult } = require("express-validator");
const { AddUser} = require("./OnlineUser")
const io = require("../../webSocketConfig");



module.exports = async (req, res, next) => {

    try {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    var userName = req.body.userName;
    
    let loadedUser = await ChatAppUser.findOne({userName: userName})

        if (loadedUser) {
            res.status(404).json({
                message: "userhandle already exist"
              });
        }
        else {
        const user = new ChatAppUser({
            userName: req.body.userName,       
        });
    
        let saveuser = await user.save()

        let opt={
            type:"add",
          _id: saveuser._id.toString(),
          userName: saveuser.userName,
        }
      
        AddUser(opt)
        const token = jwt.sign(
          {
            userId: saveuser._id.toString()
          },
          "somesupersecretsecret",
          {
            expiresIn: "100h"
          }
        );
        let userdetails = {
          _id: saveuser._id.toString(),
          userName: saveuser.userName,
          chatNotification: saveuser.chatNotification,
         
        };

        res.status(201).json({
          message: "Successfully Login",
          user: userdetails,
          token: token
        });
        io.getIO().emit('Notification/online', {
          data: opt
      })

    }
} catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
}




