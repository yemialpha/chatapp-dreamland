

const {getUser} = require("./OnlineUser")

module.exports =async (req, res, next) => {

    try {

    res.status(200).json({
            message: "Successful",
            status: "OK",
            user:getUser(),
        });
            
           
        } catch (err) {
            if (!err.statusCode) {
              err.statusCode = 500;
            }
            next(err);
        }
   
}


