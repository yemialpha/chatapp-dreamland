const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const io = require("../../webSocketConfig");
const ChatAppUser = require("../../models/ChatAppUser");
const Chat = require("../../models/Chat");



module.exports = async (req, res, next) => {

    let chatId  = ()=>{
        let arv =[`${req.userId}`, `${req.body.chatWith}`]
        let sortArv = arv.sort()
        return sortArv.join('')
        
    }
    let loadedUser = await ChatAppUser.findById(req.userId)
    let user = await ChatAppUser.findById(req.body.chatWith)

try {
        let opt = {
        _id: loadedUser._id,
        name: loadedUser.userName,
        time: Date.now(),
        avatar: ''
        }
        const chat = new Chat({
            text: req.body.text,
            user:opt,
            createdAt: new Date(),
            chatId: chatId(),
        });

        let savechat = await chat.save()
        
        res.status(201).json({
            message: "Successful",
            status: "OK",
            chat: savechat,
        });

        io.getIO().emit(`NotifyMember/${req.body.chatWith}`, {
        data: savechat
        })

        io.getIO().emit(`Notification/${chatId()}`, {
            data: savechat
        })

        const found = user.chatNotification.find(element => element.chatId == chatId());
        let count;

        if(found){
            count = found.count + 1
        }else{
            count = 1   
        }

        let chatNotificationOption ={
            sender: loadedUser.userName,
            chatId: chatId(),
            count: count
        }

        let result = user.chatNotification.filter(
            element => { console.log(element.chatId != chatId()); return element.chatId != chatId()}
            );
        result.push(chatNotificationOption)

        user.chatNotification = result
        user.save()

    } catch (err) {
        if (!err.statusCode) {
          err.statusCode = 500;
        }
        next(err);
      }
}