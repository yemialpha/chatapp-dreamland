const Dream = require("../../models/Dream");
const { check, validationResult } = require("express-validator");



module.exports = async (req, res, next) => {

    try {

      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }

        const dream = new Dream({
            fullname: req.body.fullname,
            dreamDetail: req.body.dreamDetail,
        });
     
        let savedream = await dream.save()

        res.status(201).json({
            message: "Successful",
            status: "OK",
            dream: savedream,
          });

    } catch (err) {
        if (!err.statusCode) {
          err.statusCode = 500;
        }
        next(err);
      }
}