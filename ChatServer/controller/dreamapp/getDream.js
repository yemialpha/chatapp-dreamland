const Dream = require("../../models/Dream");



module.exports = async (req, res, next) => {

    try {
        const currentPage = req.query.page || 1;
        const perPage = 5;
        let totalItems = await Dream.find().countDocuments()
        let dreams = await Dream.find().skip((currentPage - 1) * perPage).limit(perPage);

        res.status(200).json({
            message: "Successful",
            status: "OK",
            dreams: dreams,
            totalItems
          });

    } catch (err) {
        if (!err.statusCode) {
          err.statusCode = 500;
        }
        next(err);
      }
}