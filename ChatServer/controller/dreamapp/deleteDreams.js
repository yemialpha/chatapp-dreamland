const Dream = require("../../models/Dream");
const { check, validationResult } = require("express-validator");



module.exports = async (req, res, next) => {

    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(422).json({ errors: errors.array() });
        }
       
        let dreams = await Dream.findByIdAndDelete(req.body.dreamId)

        res.status(201).json({
            message: "Successfully Deleted",
            status: "OK",
            dreams: dreams,
          });

    } catch (err) {
        if (!err.statusCode) {
          err.statusCode = 500;
        }
        next(err);
      }
}