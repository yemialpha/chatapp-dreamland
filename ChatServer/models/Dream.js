const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const DreamSchema = new Schema(
  {
    fullname:{
          type: String,
          required:true
        },
    dreamDetail:{
      type: String,
      required:true
    },
    comment:[
    {
      type: Schema.Types.ObjectId,
      ref: "Comment"
    }
  ],
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("Dream", DreamSchema);
