const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const PrivateChatSchema = new Schema(
  {
    member:[
        {
          type: Schema.Types.ObjectId,
          ref: "ChatAppUser"
        }
      ],
    chat:[
        {
          type: Schema.Types.ObjectId,
          ref: "Chat"
        }
      ]
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("PrivateChat", PrivateChatSchema);
