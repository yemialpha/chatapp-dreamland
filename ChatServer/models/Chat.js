const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const PrivateChatSchema = new Schema(
  {
    text:{
          type: String,
          required:true
        },
    user:{
          type: Object,
          required:true
        },
    chatId:{
          type: String,
          required:false
        }
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("Chat", PrivateChatSchema);
