const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ChatAppUserSchema = new Schema(
  {
    userName:{
        type:String,
        required:true,
        unique:true
    },
    chatNotification:[
        {
        type:Object,
        required:false
        }
    ],
    privateChatId:[
        {
          type:String,
          required:true,
        }
    ]
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("ChatAppUser", ChatAppUserSchema);
