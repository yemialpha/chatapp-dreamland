const express = require("express");
const { check } = require("express-validator");


const dreamapp = require("../../controller/dreamapp/");

const router = express.Router();

router.post("/postDream",
    [
        check("fullname")
        .trim()
        .not()
        .isEmpty(),
        check("dreamDetail")
        .trim()
        .not()
        .isEmpty(),
    ], 
dreamapp.postDream )

router.post("/Addcomment",
    [
        check("fullname")
        .trim()
        .not()
        .isEmpty(),
        check("comment")
        .trim()
        .not()
        .isEmpty(),
        check("dreamId")
        .trim()
        .not()
        .isEmpty(),
    ], 
dreamapp.Addcomment )


router.get("/getDreams", dreamapp.getDream )
router.put("/deleteDreams",
    [
    check("dreamId")
    .trim()
    .not()
    .isEmpty(),
    ],
dreamapp.deleteDreams)

router.put("/GetComment",
    [
    check("dreamId")
    .trim()
    .not()
    .isEmpty(),
    ],
dreamapp.GetComment)





module.exports = router;