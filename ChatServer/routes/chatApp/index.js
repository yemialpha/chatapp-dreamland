const express = require("express");
const { check } = require("express-validator");
const isAuth = require("../../middleware/isAuth");


const chatApp = require("../../controller/chatApp/");

const router = express.Router();


router.get("/getAllHandles",chatApp.getAllHandles )
router.put("/logout", isAuth, chatApp.LogOut )
router.put("/getPrivatChat",isAuth, chatApp.getPrivatChat )
router.post("/getHandle",
    [
    check("userName")
    .trim()
    .not()
    .isEmpty(),
    ],

chatApp.getHandle )
router.post("/CreateHandle",
    [
    check("userName")
    .trim()
    .not()
    .isEmpty(),
    ],

chatApp.CreateHandle )
router.post("/postPrivatChat",isAuth,
    [
    check("chatWith")
    .trim()
    .not()
    .isEmpty(),
    check("text")
    .trim()
    .not()
    .isEmpty()  
    ],
chatApp.postPrivatChat )




module.exports = router;