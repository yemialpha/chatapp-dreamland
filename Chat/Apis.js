

export const Domain = "https://gravicart.com/api/v1";
export const DomainSocket = "https://gravicart.com";

//export const Domain = "http://192.168.8.100:3333/api/v1";
//export const DomainSocket = "http://192.168.8.100:3333";


export const getAllHandles = `${Domain}/chatApp/getAllHandles`;
export const getHandle = `${Domain}/chatApp/getHandle`;
export const CreateHandle = `${Domain}/chatApp/CreateHandle`;
export const getPrivatChat = `${Domain}/chatApp/getPrivatChat`;
export const postPrivatChat = `${Domain}/chatApp/postPrivatChat`;
export const logout = `${Domain}/chatApp/logout`;



