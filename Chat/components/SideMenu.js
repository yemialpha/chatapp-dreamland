import React, { useContext, useState } from "react";
import {
  StyleSheet,
  SafeAreaView,
  Image,
  ScrollView,
  Dimensions,
  TouchableOpacity,
  Share,
  AsyncStorage
} from "react-native";
import {
  List,
  ListItem,
  Left,
  Right,
  Body,
  Icon,
  Text,
  View,
  Button,
  Header,
  Thumbnail,
  Toast,
  Container
} from "native-base";
import { elevate } from "react-native-elevate";




const { height, width } = Dimensions.get("window");

export default function SideMenu(props) {

 const onShare = async () => {
    try {
      const result = await Share.share({
        message: "Wallet"
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

    return (
      <Container style={{ backgroundColor: "#fff" }}>
        <View
          style={{
            marginTop: 15,
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center"
          }}
        >
          <View>
            <Text
              style={{
                color: "#181818",
                fontWeight: "bold",
                marginHorizontal: 10,
                fontSize: 15
              }}
            >
              Golegit
            </Text>
          </View>

          <View>
            <Button
              onPress={() => {
                props.navigation.closeDrawer();
              }}
              style={{ marginLeft: 20 }}
              transparent
            >
              <Icon style={{ color: "#181818", fontSize: 30 }} name='close' />
            </Button>
          </View>
        </View>
        <ScrollView>
          <View
            style={{
              marginBottom: 20,
              height: height / 3.5,
              backgroundColor: "#d9096a",
              paddingHorizontal: 5
            }}>
            <View style={{flex:3, justifyContent: "flex-end", alignItems: 'center', }}> 
              <TouchableOpacity onPress={() => props.navigation.navigate("Profile")}>
                <View
                  style={[
                    {
                      height: height / 9,
                      width: height / 9,
                      backgroundColor: "white",
                      justifyContent: "center",
                      alignItems: "center",
                      borderRadius: 60
                    },
                    elevate(4)
                  ]}
                >
                </View>
              </TouchableOpacity>
            </View>
            <View style={{flex:1,margin:20, justifyContent: 'flex-start', alignItems: 'center', }}> 
              <Text>HHH</Text>
            </View>
          </View>
          <List>
            <ListItem
              icon
              onPress={() => props.navigation.navigate("home")}
            >
              <Left>
              <Button style={{ backgroundColor: "#d9096a" }}>
                <Icon type='AntDesign' active name="home" />
              </Button>
              </Left>
              <Body>
                <Text style={{ color: "#181818" }}>Home</Text>
              </Body>
              <Right></Right>
            </ListItem>
            <ListItem
              icon
              onPress={() => props.navigation.navigate("wallet")}
            >
              <Left>
              <Button style={{ backgroundColor: "#d9096a" }}>
                <Icon type='Entypo' active name="wallet" />
              </Button>
              </Left>
              <Body>
                <Text style={{ color: "#181818" }}>Fund Wallet</Text>
              </Body>
              <Right></Right>
            </ListItem>
            <ListItem
              icon
              onPress={() => props.navigation.navigate('changePassword')
              }
            >
              <Left>
              <Button style={{ backgroundColor: "#d9096a" }}>
                <Icon type='MaterialCommunityIcons' active name="account-key" />
              </Button>
              </Left>
              <Body>
                <Text style={{ color: "#181818" }}>Change Password</Text>
              </Body>
              <Right></Right>
            </ListItem>
          </List>
          <View
            style={{
              borderBottomColor: "#181818",
              marginTop: 30,
              marginBottom: 30,
              margin: 30,
              borderBottomWidth: 1,
              width: width / 2
            }}
          ></View>
          <List>
            <ListItem
              icon
              onPress={() => props.navigation.navigate('SettingScreen')}
            >
              <Left>
              <Button style={{ backgroundColor: "#d9096a" }}>
                <Icon type='Feather' active name="settings" />
              </Button>
              </Left>
              <Body>
                <Text style={{ color: "#181818" }}>Settings</Text>
              </Body>
              <Right></Right>
            </ListItem>
            <ListItem  icon onPress={() => onShare()}>
              <Left>
              <Button style={{ backgroundColor: "#d9096a" }}>
                <Icon type='Entypo' active name="share" />
              </Button>
              </Left>
              <Body>
                <Text style={{ color: "#181818" }}>Share this App</Text>
              </Body>
              <Right></Right>
            </ListItem>
          </List>
        </ScrollView>
      </Container>
    );
  }

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  imageUser: {
    height: height / 3 - 20,
    alignItems: "center",
    justifyContent: "center"
  }
});
