import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Dashboard  from "../screens/Dashboard";
import PrivateMessage  from "../screens/PrivateMessage";






const Stack = createStackNavigator();


function MyStack() {
  return (
    <NavigationContainer>
    <Stack.Navigator  
    screenOptions={{
        cardOverlayEnabled: true,
        cardStyleInterpolator: ({ current: { progress } }) => ({
        cardStyle: {
            opacity: progress.interpolate({
            inputRange: [0, 0.5, 0.9, 1],
            outputRange: [0, 0.25, 0.7, 1]
            })
        },
        overlayStyle: {
            opacity: progress.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 0.5],
            extrapolate: "clamp"
            })
        }
        })
    }}>
      <Stack.Screen options={({ route }) => ({ headerShown: false })} name="Dashboard" component={Dashboard} />
      <Stack.Screen options={({ route }) => ({ headerShown: false })} name="PrivateMessage" component={PrivateMessage} />
    </Stack.Navigator>
  </NavigationContainer>
  );
}



export default MyStack;




