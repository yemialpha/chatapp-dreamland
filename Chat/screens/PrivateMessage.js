import React, { useState, useCallback, useEffect } from 'react'
import {Dimensions} from 'react-native';
import { GiftedChat } from 'react-native-gifted-chat'
import { View,Button,Header,Left,Icon,Body,Container,Title,Right,Text } from "@red-elephant/native-base";
import axios from "axios";
import openSocket from "socket.io-client";
import {DomainSocket, getPrivatChat, postPrivatChat } from "../Apis";
const { width, height } = Dimensions.get("window");

export default function Privatechat(props) {
  const [messages, setMessages] = useState([]);
  const [item, setItem]= useState(props.route.params.item)
  const [token, setToken]= useState(props.route.params.token)
  const [user, setUser]= useState(props.route.params.user)
  const [loading, setLoading]= useState(false)
  const [serverMessage, setServerMessage]= useState('')


 
  useEffect(() => {
    GetChat()
    let chatId  = ()=>{
      let arv =[`${user._id}`, `${item._id}`]
      let sortArv = arv.sort()
      return sortArv.join('')
      
  }
    const socket = openSocket(`${DomainSocket}`);
    socket.on(`Notification/${chatId()}`, data => {
      console.log(data.data, "IO")
      if(JSON.stringify(data.data.user._id) != JSON.stringify(user._id)){
        setMessages(previousMessages => GiftedChat.append(previousMessages, data.data))
      }
    });
  }, [])

  const GetChat=()=>{
    setServerMessage('')
     setLoading(true);
     axios.put(`${getPrivatChat}`,{
       chatWith: item._id
      }, { 
      headers: { Authorization: "Bearer " + token } 
     })
     .then(res => {
       setMessages(res.data.chat)
     })
     .catch(err => {
       console.log(err)
       //setServerMessage(err.response.data.message)
     }).finally(()=>{
       setLoading(false);
     })
}

const SendChat=(message)=>{
  setServerMessage('')
   axios.post(`${postPrivatChat}`,{
     chatWith: item._id,
     text:message[0].text
    }, { 
    headers: { Authorization: "Bearer " + token } 
   })
   .then(res => {
    setMessages(previousMessages => GiftedChat.append(previousMessages, message)) 
   })
   .catch(err => {
     console.log(err)
     //setServerMessage(err.response.data.message)
   }).finally(()=>{
     setLoading(false);
   })
}
 
  const onSend = useCallback((messages = []) => {
    console.log(messages[0], "messages")
    SendChat(messages)
    //setMessages(previousMessages => GiftedChat.append(previousMessages, messages))
  }, [])
 
  return (
    <Container style={{backgroundColor:'#f1f2f3'}}>
    <Header style={{backgroundColor:'#6b7db3'}}>
    <Left>
      <Button onPress={()=> props.navigation.pop()} transparent>
        <Icon name='arrow-back' />
        <Text>Back</Text>
      </Button>
    </Left>
    <Body/>
    <Right>
      <Text style={{color: 'white',textTransform:'uppercase'}}>Your Conversations With {item.userName}</Text>
    </Right>
  </Header>
    <View style={{flex: 1, width:width/2,backgroundColor:'#fff',borderTopLeftRadius:50, borderTopRightRadius:50, }}>
    <GiftedChat
      messages={messages}
      onSend={messages => onSend(messages)}
      user={{
        _id: 1,
        name: 'React Yemi',
        avatar: 'https://placeimg.com/140/140/any',
      }}
    />
  </View>
    </Container>
  )
}