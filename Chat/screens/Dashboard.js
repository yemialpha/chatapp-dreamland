import React, { useState, useEffect } from 'react';
import {Dimensions,TouchableOpacity,FlatList} from 'react-native';
import {Container, 
        Header, 
        Left,
        Body, 
        Right, 
        Button,
        Text, 
        Title, 
        View,
        Icon,
        List,
        ListItem,
        Thumbnail,
        Form,
        Toast,
        Input,
        Label,
        Item,
        CardItem,
        Switch,
        Spinner
      } from "@red-elephant/native-base";
import axios from "axios";
import dayjs from "dayjs";
import openSocket from "socket.io-client";
import {DomainSocket, getAllHandles, getHandle, CreateHandle,logout } from "../Apis";

const { width, height } = Dimensions.get("window");


export default function Dashboard(props) {
const [modalVisible, setmodalVisible]= useState(false)
const [loading, setLoading]= useState(false)
const [serverMessage, setServerMessage]= useState('')
const [allUser, setUsers]= useState([])
const [User, setUser]= useState('')
const [userName, setuserName]= useState('')
const [token, setToken]= useState('')


useEffect(() => {
       Getusers()
       const socket = openSocket(`${DomainSocket}`);
       socket.on('connect', function(sockets){
        console.log('a user connected');
        console.log(sockets);
        });
       socket.on('Notification/online', data => {
        Getusers()
        })

  }, []);

  const DisplayTime=()=>{             
    return dayjs().format('MMM D, YYYY')
  }



  const Getusers=()=>{
    setServerMessage('')
     setLoading(true);
     axios.get(`${getAllHandles}`)
     .then(res => {
       console.log(res.data.user)
       setUsers(res.data.user)
     })
     .catch(err => {
       console.log(err)
       setServerMessage(err.response.data.message)
     }).finally(()=>{
       setLoading(false);
     })
  }

  const LogOut=()=>{
    setServerMessage('')
     setLoading(true);
     axios.put(`${logout}`,{},
     { 
      headers: { Authorization: "Bearer " + token } 
     }
     )
     .then(res => {
       console.log(res.data)
       setUser('')
     })
     .catch(err => {
       console.log(err)
       setServerMessage(err.response.data.message)
     }).finally(()=>{
       setLoading(false);
     })
  }

  const gethandle=()=>{
    if(userName == ''){
      return
    }
    setServerMessage('')
     setLoading(true);
     axios.post(`${getHandle}`, {userName:userName},)
     .then(res => {
       console.log(res.data.user)
       setUser(res.data.user)
       setToken(res.data.token)
       setmodalVisible(false)
     })
     .catch(err => {
       console.log(err)
       setServerMessage(err.response.data.message)
     }).finally(()=>{
       setLoading(false);
     })
  }

  const createhandle=()=>{
    if(userName == ''){
      return
    }
    setServerMessage('')
     setLoading(true);
     axios.post(`${CreateHandle}`, {userName:userName},)
     .then(res => {
       console.log(res.data.user)
       setUser(res.data.user)
       setToken(res.data.token)
       setmodalVisible(false)
     })
     .catch(err => {
       console.log(err)
       setServerMessage(err.response.data.message)
     }).finally(()=>{
       setLoading(false);
     })

  }

  const tran=(item)=>{
    return(
      <List style={{width:width/2.5}} noBorder>
      <ListItem onPress={()=>{ 
        if(User == ''){
          Toast.show({
            text: "Join",
            buttonText: "Okay",
            position: "top",
            duration: 3000
          })
          return
        }
        if(JSON.stringify(User._id) != JSON.stringify(item._id)){
          props.navigation.push("PrivateMessage", {item:item, token:token, user:User}) 
        }else{
          Toast.show({
            text: "You are not allow to chat with yourself",
            buttonText: "Okay",
            position: "top",
            duration: 3000
          })
        }
      }
      }  
        avatar>
        <Left style={{marginRight:60}}>
          <Thumbnail style={{backgroundColor:'#181818'}} source={{ uri: 'https://cdn.vuetifyjs.com/images/lists/1.jpg' }} />
        </Left>
        <Body>
          <Text>{item.userName}</Text>
          <Text note>Send Direct Message</Text>
        </Body>
        <Right>
          <Text note>online</Text>
          <View style={{height:10, width:10, borderRadius:20, backgroundColor:'green'}}/>
        </Right>
      </ListItem>
    </List>
    )
  }

    return (
      <Container>
      <Header style={{backgroundColor:'#6b7db3'}}>
        <Left>
          <Button transparent>
          <Icon style={{color:'#red'}} type='FontAwesome' name='long-arrow-left'/>
          </Button>
        </Left>
        <Body>
          <Title>Join The Chat</Title>
        </Body>
        <Right>
          <Button transparent>
          {loading ? (<Spinner color='white' />):null}
          </Button>
        </Right>
      </Header>
      <View style={{justifyContent:'center',alignItems:'center', backgroundColor:'#fff'}}>
      {allUser.length > 0 ? 
         (
         <FlatList
            onRefresh={() => Getusers()}
            refreshing={loading}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            data={allUser}
            renderItem={({ item, index }) => tran(item, index)}
            keyExtractor={item => item._id}
          />
          ):(
            <Text style={{color:'#0d3575', textAlign:'center',marginTop:20, fontWeight:"bold", fontSize:25}}>No User Online</Text>
          )}
        </View>
      {User?(
        <View style={{height:height/1.5,borderTopLeftRadius:50, borderTopRightRadius:50, width:width/4,alignItems:'center',backgroundColor:'#f1f2f3', position:"absolute",bottom:20,left:10}}>
         <View style={{backgroundColor:'white',margin:40,  height:170, width:"90%"}}>
            <CardItem>
              <Left>
              <Thumbnail style={{backgroundColor:'#181818'}} source={{ uri: 'Image URL' }} />
                <Body>
                    <Text>{User.userName}</Text>
                    <Text note>{DisplayTime()}</Text>
                </Body>
              </Left>
            </CardItem>
           <Button style={{margin:5,marginTop:20, borderRadius:5, backgroundColor:"#9999ff"}} block>
            <Text>{User.chatNotification.length} Private Message(s)</Text>
          </Button>
         </View>
         <View style={{backgroundColor:'white',margin:40,  height:150, width:"90%"}}>
         <ListItem icon>
            <Left/>
            <Body>
              <Text>Push Notification</Text>
            </Body>
            <Right>
              <Switch value={false} />
            </Right>
          </ListItem>
          <ListItem icon>
            <Left/>
            <Body>
              <Text>Notification</Text>
            </Body>
            <Right>
              <Switch value={true} />
            </Right>
          </ListItem>
          <Button onPress={()=>{LogOut()}} style={{margin:5,borderRadius:5, backgroundColor:"#e6ecff"}} block info>
            <Text style={{color:"#181818"}}>Sign out</Text>
          </Button>
        </View>
        </View>
      ):
      
      (
        <TouchableOpacity onPress={()=>setmodalVisible(!modalVisible)} style={{height:100, width:100,justifyContent:"center",alignItems:'center', borderRadius:100, backgroundColor:'#9999ff', position:"absolute",bottom:20,right:10}}>
           {!modalVisible?(
               <Text style={{color: '#fff'}}>Join</Text>
           ):(
               <Text style={{color: '#fff'}}>Close</Text>
           )}
        </TouchableOpacity>
      )
      }
  {modalVisible?
     (   
       <View style={{height:height/1.5,borderTopLeftRadius:50, borderTopRightRadius:50, width:width/4,justifyContent:"center",alignItems:'center',backgroundColor:'#f1f2f3', position:"absolute",bottom:20,left:10}}>
        <Thumbnail large style={{backgroundColor:'#ff9999',marginBottom:30}} source={{ uri: 'Image URL' }} />
        <Text style={{color:"#181818", textAlign: "center",fontSize:16, fontWeight:'600',marginBottom:40}}>Provide User Handle</Text>
        <Text style={{color:"red"}} note>{serverMessage}</Text>
      <Form>
      <Label style={{color:"#000000",marginBottom:10}}>UserName</Label>
      <Item style={{borderRadius:10}} regular>
          <Input onChangeText={(text)=>setuserName(text)} placeholder='@UserName' />
      </Item>
      <Button onPress={()=>gethandle()} style={{marginTop:30, backgroundColor:"#9999ff"}} block>
            <Text>Join</Text>
      </Button>
      <Button onPress={()=>createhandle()} style={{marginTop:30, backgroundColor:"#6b7db3"}} block>
            <Text>Join With New Handle</Text>
      </Button>
      </Form>
      </View>
    ):null}
    </Container>
    );
  
}