import { StatusBar } from 'expo-status-bar';
import React from 'react';
import NavigationContainer from './navigation/index'
import { StyleSheet, Text, View } from 'react-native';
import { Root } from "@red-elephant/native-base"

export default function App() {
  return (
    <View style={styles.container}>
      <Root>
        <NavigationContainer/>
        </Root> 
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
});
