import Vue from 'vue'
import Vuex from 'vuex'
import Dream from './Dream'



Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
    Dream
    },
    strict: process.env.NODE_ENV !== 'production'
})