import axios from "axios";
import {postDream, getDreams, deleteDreams} from "../APIs";

const state = {
    Dreams:[],
    loading: false,
    serverMessage:"",
    snackbar: true,
    modal: false,
    TotalPage:0

};

const getters = {
    getDreams(state){
        return state.Dreams
    },
    getModal(state){
      return state.modal
  },
};

const mutations = {
  setDream(state, payload) {
    state.Dreams = payload;
  },
  setLoading(state, payload){
    state.loading = payload;
  },
  addDream(state, payload){
   let newDate = state.Dreams
    newDate.push(payload)
    newDate.sort(function(a,b){
      return new Date(b.createdAt) - new Date(a.createdAt);
    });
   state.Dreams = newDate
   state.modal = false

  },

  removeDream(state, payload){
   let data = state.Dreams.filter(item => item._id != payload._id);
    state.Dreams = data
  },

  setServerMessage(state, payload){
    state.serverMessage = payload
  },
  setSnackbar(state, payload){
    state.snackbar = payload
  },
  setModal(state, payload){
    state.modal = payload
  },
  setTotalPage(state, payload){
    state.TotalPage = payload
  },
 
};

const actions = {

    SetDream({commit}, payload){
    commit("setLoading", true);
    axios
      .post(`${postDream}`,payload)
      .then(res => {
        commit("addDream", res.data.dream);
        
      })
      .catch(err => {
        commit("setSnackbar", true);
        commit("setServerMessage", err.message)
      })
      .finally(()=>{
        commit("setLoading", false);
      })
  },

  GetDreams({commit},payload){
    commit("setLoading", true);
    axios
      .get(`${getDreams}?page=${payload.page}`)
      .then(res => {
        console.log(res.data)
            commit("setDream", res.data.dreams);
            commit("setTotalPage", res.data.totalItems);

      })
      .catch(err => {
        console.log(err)
      })
      .finally(()=>{
        commit("setLoading", false);
      })
  },

  DeleteDreams({commit}, payload){
    commit("setLoading", true);
    axios
      .put(`${deleteDreams}`, payload)
      .then(res => {
        commit("removeDream", res.data.dreams);
      })
      .catch(err => {
        console.log(err)

      })
      .finally(()=>{
        commit("setLoading", false);
      })
  },
};

export default {
  state,
  mutations,
  getters,
  actions
};








