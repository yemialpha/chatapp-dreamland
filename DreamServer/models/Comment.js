const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const CommentSchema = new Schema(
  {
    fullname:{
          type: String,
          required:true
        },
    comment:{
        type: String,
        required:true
    },
    dreamId:{
        type: Schema.Types.ObjectId,
        ref: "Dream"
    }
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("Comment", CommentSchema);
