const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const ChatAppUser = require("../../models/ChatAppUser");
const { check, validationResult } = require("express-validator");



module.exports = async (req, res, next) => {

    try {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    var userName = req.body.userName;
    
    let loadedUser = await ChatAppUser.findOne({userName: userName})

        if (!loadedUser) {
          res.status(404).json({
            message: "No user found"
          });
        }
        else {
        const token = jwt.sign(
          {
            userId: loadedUser._id.toString()
          },
          "somesupersecretsecret",
          {
            expiresIn: "100h"
          }
        );
        let user = {
          _id: loadedUser._id.toString(),
          userName: loadedUser.userName,
          chatNotification: loadedUser.chatNotification,
         
        };

        res.status(201).json({
          message: "Successfully Login",
          user: user,
          token: token
        });

    }
} catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
}