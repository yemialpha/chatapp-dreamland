const ChatAppUser = require("../../models/ChatAppUser");
const { check, validationResult } = require("express-validator");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");


module.exports =async (req, res, next) => {

    try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }


    let users = await ChatAppUser.find();

    res.status(200).json({
            message: "code sent",
            status: "OK",
            user:users,
        });
            
           
        } catch (err) {
            if (!err.statusCode) {
              err.statusCode = 500;
            }
            next(err);
        }
   
}


