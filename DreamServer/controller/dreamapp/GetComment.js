const Comment = require("../../models/Comment");
const { check, validationResult } = require("express-validator");



module.exports = async (req, res, next) => {

    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(422).json({ errors: errors.array() });
        }
       
        let comments = await Comment.find({dreamId:req.body.dreamId})

        res.status(201).json({
            message: "Successfully",
            status: "OK",
            comments: comments,
          });

    } catch (err) {
        if (!err.statusCode) {
          err.statusCode = 500;
        }
        next(err);
      }
}