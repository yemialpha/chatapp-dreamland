const Dream = require("../../models/Dream");
const Comment = require("../../models/Comment");
const { check, validationResult } = require("express-validator");



module.exports = async (req, res, next) => {

    try {

      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }
      let dream = await Dream.findById(req.body.dreamId)
      if (!dream) {
        return res.status(404).json({
          message: "no dream found",
          status: "fail"
        });
      }
        const comment = new Comment({
            fullname: req.body.fullname,
            comment: req.body.comment,
            dreamId: req.body.dreamId,
        });
     
        let saveComment = await comment.save()
        dream.comment.push(saveComment)
        dream.save()

        res.status(201).json({
            message: "Successful",
            status: "OK",
            dream: saveComment,
          });

    } catch (err) {
        if (!err.statusCode) {
          err.statusCode = 500;
        }
        next(err);
      }
}