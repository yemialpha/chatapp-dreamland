const fs = require("fs");
const path = require("path");

const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require("cors");
const morgan = require("morgan");
const helmet = require("helmet");

const chatApp = require("./routes/chatApp");
const dream = require("./routes/dreamApp");


const app = express();

const accessLogStream = fs.createWriteStream(
  path.join(__dirname, "Request.log"),
  {
    flags: "a"
  }
);

app.use(helmet());
app.use(
  morgan("combined", {
    stream: accessLogStream
  })
);

// middleware
app.use(cors());
app.use(bodyParser.json());

//public path
app.use(express.static(path.join(__dirname, "public")));

//APIs endPoint
app.use("/api/v1/chatApp", chatApp);
app.use("/api/v1/dream", dream);




app.use((error, req, res, next) => {
    console.log(error);
    const status = error.statusCode || 500;
    const message = error.message;
    const data = error.data;
    res.status(status).json({
      message: message,
      status: "fail",
      data: data
    });
});

const PORT = process.env.PORT || 3330;
mongoose.connect("mongodb://localhost/generaldata",
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true
      }
    )
    .then(() => {
      const server = app.listen(PORT, () => {
        console.log(`Dream server running on ${PORT}`);
      });
      const io = require("./webSocketConfig").init(server);
      io.on("connection", socket => {
        console.log("Client connected");
      });
    })
    .catch(err => {
      console.log(err);
});